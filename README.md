# Live Workspaces
Adds a feature to distinguish between live workspaces (workspaces that can be accessed by an end user) and workspaces used for other purposes (like preparing content).

The module adds a field called 'live' to the workspace entities. For each request, if the user does not have the permission to bypass the live workspace access check, in case the current workspace is not a live workspace, a 404 page is returned.