<?php

namespace Drupal\live_workspaces\EventSubscriber;

use Drupal\workspaces\WorkspaceManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class LiveWorkspacesRequestSubscriber implements EventSubscriberInterface {

  /**
   * @var WorkspaceManagerInterface
   *
   * The workspace manager service.
   */
  protected $workspaceManager;

  /**
   * LiveWorkspacesRequestSubscriber constructor.
   * @param WorkspaceManagerInterface $workspaceManager
   */
  public function __construct(WorkspaceManagerInterface $workspaceManager) {
    $this->workspaceManager = $workspaceManager;
  }

  public function onKernelRequestCheckWorkspaceAccess(GetResponseEvent $event) {
    // Abort if the current request already has an exception.
    $exception = $event->getRequest()->attributes->get('exception');
    if ($exception instanceof NotFoundHttpException) {
      return;
    }

    $workspace = $this->workspaceManager->getActiveWorkspace();
    if (\Drupal::currentUser()->hasPermission('bypass live workspace access check')) {
      return;
    }

    if (!empty($workspace->live->value)) {
      return;
    }

    // If we are here it means the workspaces is not a live one and the user
    // does not have permission to see it, so just throw a 404.
    throw new NotFoundHttpException();
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onKernelRequestCheckWorkspaceAccess'];
    return $events;
  }
}
